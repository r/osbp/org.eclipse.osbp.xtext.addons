/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.addons

import javax.inject.Inject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.xbase.compiler.JvmModelGenerator
import org.eclipse.xtext.xbase.jvmmodel.JvmAnnotationReferenceBuilder
import org.eclipse.xtext.xbase.jvmmodel.JvmTypeReferenceBuilder

class AdvancedJvmModelGenerator extends JvmModelGenerator {
	@Inject JvmAnnotationReferenceBuilder.Factory annotationRefBuilderFactory;
	@Inject JvmTypeReferenceBuilder.Factory typeRefBuilderFactory;
	
	@Extension protected JvmAnnotationReferenceBuilder _annotationTypesBuilder;
	@Extension protected JvmTypeReferenceBuilder _typeReferenceBuilder;
	
	def void setBuilder(Resource resource) {
			_annotationTypesBuilder = annotationRefBuilderFactory.create(resource.getResourceSet());
			_typeReferenceBuilder = typeRefBuilderFactory.create(resource.getResourceSet());
	}
		
}
