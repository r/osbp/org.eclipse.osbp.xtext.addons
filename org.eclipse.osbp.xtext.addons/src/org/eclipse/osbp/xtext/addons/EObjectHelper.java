/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.addons;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.xtext.oxtype.resource.ISemanticLoadingResource;

public class EObjectHelper {

	public static EObject getSemanticElement(Resource resource) {
		if (resource instanceof ISemanticLoadingResource) {
			return ((ISemanticLoadingResource) resource).getSemanticElement();
		} else {
			return resource.getContents().get(0);
		}
	}

	public static EObject getSemanticElement(Resource resource, String fragment) {
		if (resource instanceof ISemanticLoadingResource) {
			return ((ISemanticLoadingResource) resource)
					.getSemanticElement(fragment);
		} else {
			return resource.getEObject(fragment);
		}
	}

}
